<?php

?>
<!DOCTYPE html>
<html>
<head>
    <title>
        Purchase - Details
    </title>
</head>
<body>
    <form action="./pay.php" method="POST">
    <table align=center>   
        <tr>
            <td colspan=2 align=center>
            <?php
                //This will insert an image on the table to specify which payment method it is
                switch($_GET["payment_option"])
                {
                    case "visa":
                    {
                        echo "<img src='./images/payment/visa.jpg' width=120 height=60 />";
                        break;
                    }
                    case "paypal":
                    {
                        echo "<img src='./images/payment/paypal.jpg' width=120 height=60 />";
                        break;
                    }
                    case "mastercard":
                    {
                        echo "<img src='./images/payment/mastercard.jpg' width=120 height=60 />";
                        break;
                    }
                    
                }
            ?>
            </td>
        </tr>
        <tr>
            <td>Card Number:</td>
            <td><input type="text" pattern="[0-9]{10}" name="card_number" title="Should be a valid card number!"/>
        </tr>
        <tr>
            <td>CardHolder Name:</td>
            <td><input type="text" name="cardholder_name" />
        </tr>
        <tr>
            <td>Expiry:</td>
            <td><input type="text" pattern="[0-9]{2}"  title="Please enter a valid month" name="expiry_month" style='width: 50px;'/> / <input type="text" pattern="[0-9]{4}"  title="Please enter a valid year" name="expiry_year" style='width: 102.5px;'/></td>
        </tr>
        <tr>
            <td colspan=2 align=center>
                <button>
                    Pay
                </button>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>