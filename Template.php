<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><?php echo $title; ?></title>
		<link rel="Stylesheet" type="text/css" href="Style/Stylesheet.css"/>
    </head>
    <body>
        <div id="wrapper">
            <div id="banner">             
            </div>
            
            <nav id="navigation">
                <ul id="nav">
                    <li><a href="index.php">Home</a></li>
                    <li><a href="product.php">Products</a></li>
                    <li><a href="team.php">Team</a></li>
                    <li><a href="contact.php">Contact us</a></li>
					<li><a href="login.php">Login</a></li>
					<li><a href="Register.php">Register</a></li>
					
                </ul>
            </nav>
	
            
            <div id="content_area">
                <?php echo $content; ?>
            </div>
            
            <div id="sidebar">
                
            </div>
            
            <footer>
                <p>All RIGHTS RESERVED</p>
            </footer>
        </div>
    </body>
</html>
