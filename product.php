<!DOCTYPE html>
<?php

$title = "Products";
$content = '

<head>
<style>
div.img {
    margin: 35px;
    border: 1px solid #ccc;
    float: right;
    width: 250px;
}

div.img:hover {
    border: 1px solid #000;
}

div.img img {
    width: 100%;
    height: auto;
}

div.desc {
    padding: 15px;
    text-align: center;
}

</style>
<script>
function redirectPay(id)
{
	window.location = "./payment.php?prodid="+id;
}
</script>
</head>
<body>
<div class="img"><figure>
    <img src="Images/Sports/ball3.jfif">
	</a><figcaption align="center">Maroon Cork Ball<br>Price:RS 100</figcaption></figure>
	<button onClick="redirectPay(0);">Buy</button>
</div>

<div class="img"><figure>
    <img src="Images/Sports/ball5.jfif">
	</a><figcaption align="center">White Cork Ball<br>Price:RS 150</figcaption></figure>
	<button onClick="redirectPay(1);">Buy</button>
</div>

<div class="img"><figure>
    <img src="Images/Sports/ball4.jfif">
	</a><figcaption align="center">Red Cork Ball<br>Price:RS 120</figcaption></figure>
	<button onClick="redirectPay(2);">Buy</button>
</div>

<div class="img"><figure>
    <img src="Images/Sports/bat1.jpg">
	</a><figcaption align="center">Kingfisher Bat<br>Price:RS 1000</figcaption></figure>
	<button onClick="redirectPay(3);">Buy</button>
</div>

<div class="img"><figure>
    <img src="Images/Sports/bat2.jpg">
	</a><figcaption align="center">MRF Bat<br>Price:RS 2000</figcaption></figure>
	<button onClick="redirectPay(4);">Buy</button>
</div>

<div class="img"><figure>
    <img src="Images/Sports/bat3.jpg">
	</a><figcaption align="center">SS Bat<br>Price:RS 900</figcaption></figure>
</div>

<div class="img"><figure>
    <img src="Images/Sports/cricket jersey1.jfif">
	</a><figcaption align="center">Netherlands Jersey<br>Price:RS 500</figcaption></figure>
</div>

<div class="img"><figure>
    <img src="Images/Sports/fbkit1.jpg">
	</a><figcaption align="center">Argentina jersey&shorts<br>Price:RS 700</figcaption></figure>
</div>

<div class="img"><figure>
    <img src="Images/Sports/fbkit2.jpg">
	</a><figcaption align="center">Barcelona Kit<br>Price:RS 1000</figcaption></figure>
</div>

<div class="img"><figure>
    <img src="Images/Sports/glove3.jfif">
	</a><figcaption align="center">Hicko Batting Gloves<br>Price:RS 500</figcaption></figure>
</div>

<div class="img"><figure>
    <img src="Images/Sports/gloves1.jfif">
	</a><figcaption align="center">MRF Batting Gloves<br>Price:RS 600</figcaption></figure>
</div>

<div class="img"><figure>
    <img src="Images/Sports/gloves2.jfif">
	</a><figcaption align="center">BDM Batting Gloves<br>Price:RS 400</figcaption></figure>
</div>

<div class="img"><figure>
    <img src="Images/Sports/helmet1.jfif">
	</a><figcaption align="center">MRF Blue Helmet<br>Price:RS 450</figcaption></figure>
</div>

<div class="img"><figure>
    <img src="Images/Sports/guard2.jfif">
	</a><figcaption align="center">Proflex L-Guard<br>Price:RS 250</figcaption></figure>
</div>

<div class="img"><figure>
    <img src="Images/Sports/helmet3.jfif">
	</a><figcaption align="center">Masuri Helmet<br>Price:RS 400</figcaption></figure>
</div>

<div class="img"><figure>
    <img src="Images/Sports/pad1.jfif">
	</a><figcaption align="center">Proflex Leg Guard<br>Price:RS 600</figcaption></figure>
</div>

<div class="img"><figure>
    <img src="Images/Sports/pad3.jfif">
	</a><figcaption align="center">Campus Leg Guard<br>Price:RS 550</figcaption></figure>
</div>

<div class="img"><figure>
    <img src="Images/Sports/racket2.jfif">
	</a><figcaption align="center">MRF Racquet<br>Price:RS 900</figcaption></figure>
</div>


<div class="img"><figure>
    <img src="Images/Sports/stud3.jfif">
	</a><figcaption align="center">Adidas Studs<br>Price:RS 3500</figcaption></figure>
</div>

<div class="img"><figure>
    <img src="Images/Sports/tball1.jfif">
	</a><figcaption align="center">Tennis Ball<br>Price:RS 50</figcaption></figure>
</div>

<div class="img"><figure>
    <img src="Images/Sports/ttball2.jfif">
	</a><figcaption align="center">Stiga Table-Tennis Ball<br>Price:RS 15</figcaption></figure>
</div>


</body>
<p>
</p>';


include 'Template.php';

?>
