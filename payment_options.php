<?php  

?>
<!DOCTYPE html>
<html>
    <head>
        <title>
            Purchase Details
        </title>
    </head>
    <body>
        <form action="./payment_final.php">
        <table align=center>
            <tr>
                <td colspan=2 align=center>
                    <h1 style='font-family: Arial; font-weight: 100;'>Payment Options</h1>
                </td>
            </tr>
            <tr>
                <td>
                    <input type="radio" name="payment_option" value="visa" />
                </td>
                <td>
                    <img src="./images/payment/visa.jpg" width=120 height=60/>
                </td>
            </tr>
            <tr>
                <td>
                    <input type="radio" name="payment_option" value="mastercard" />
                </td>
                <td>
                    <img src="./images/payment/mastercard.jpg" width=120 height=60/>
                </td>
            </tr>
            <tr>
                <td>
                    <input type="radio" name="payment_option" value="paypal" />
                </td>
                <td>
                    <img src="./images/payment/paypal.jpg" width=120 height=60/>
                </td>
            </tr>
            <tr>
                <td colspan=2 align=center>
                    <button>
                        Pay
                    </button>
                </td>
            </tr>
        </table>
        </form>
    </body>
</html>